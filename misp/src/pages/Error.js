import React from "react";
import { Link } from "react-router-dom";

export default function Error() {
  return (
    <section className="Error Page ">
      <div className="Error-Container">
        <h1>Oops! Wrong Entry</h1>
        <Link to="/" className="btn btn-primary">
          Return Home
        </Link>
      </div>
    </section>
  );
}
