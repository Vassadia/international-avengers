import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWaze } from "@fortawesome/free-brands-svg-icons";

export default function About() {
  return (
    <section className="section about-section">
      <h1 className="section-title"> About Us </h1>
      <p>
        {" "}
        Sabacas Store is an beautiful arbiter of beautifully well-imagined works
        from the creative minds of many inspiring craftsmen, bringing
        thoughtful, functional design to our local community and abroad. With
        encouragement from friends and neighbors in the Outer Sunset
        neighborhood of San Francisco, our first store was founded in 2009 as a
        collaboration between Serena and Sara.{" "}
      </p>
      <center>
        <h1 className="section-nap"> Visit Us Here </h1>

        <a
          href="https://g.page/eatoncrest-apartment-homes?share"
          className="map-marker-alt"
        >
          <FontAwesomeIcon icon={faWaze} size="3x" />
        </a>
      </center>
    </section>
  );
}
