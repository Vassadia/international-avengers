import React from "react";
import { ProductContext } from "../../context/products";
const Filters = () => {
  const {
    filters: { search, category, shipping, price },
    updateFilters,
    sorted
  } = React.useContext(ProductContext);
  return (
    <section className="filters-section">
      <h5 className="section-title">search filter</h5>
      <form className="filters-form">
        {/* search input */}

        <div className="form-group">
          <label htmlFor="search"> search term</label>
          <input
            type="text"
            id="search"
            name="search"
            value={search}
            onChange={updateFilters}
            className="form-control"
          />
        </div>

        <div className="price-group">
          <p>price</p>
          <label>
            <input
              type="radio"
              name="price"
              value="all"
              checked={price === "all"}
              onChange={updateFilters}
            />
            all
          </label>
          <label>
            <input
              type="radio"
              name="price"
              value="0"
              checked={price === 0}
              onChange={updateFilters}
            />
            $0 - $300
          </label>
          <label>
            <input
              type="radio"
              name="price"
              value="300"
              checked={price === 300}
              onChange={updateFilters}
            />
            $300 - $650
          </label>
          <label>
            <input
              type="radio"
              name="price"
              value="600"
              checked={price === 600}
              onChange={updateFilters}
            />
            Over $600
          </label>
        </div>
      </form>
      <p>
        <h3>total products : {sorted.flat().length}</h3>
        <hr />
      </p>
    </section>
  );
};

export default Filters;
