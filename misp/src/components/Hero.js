import React from "react";

export default function Hero({ children }) {
  return (
    <div className="hero">
      <div className="banner">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="caption">
                <div className="line-dec"></div>
                <p> A NEW WAY FOR SHOPPING</p>
                <h3 className="section-nap1"> Order online and pick up!</h3>
                {children}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
